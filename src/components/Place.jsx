import React, {Component, PropTypes} from 'react'

import './styles/place.css'

const propTypes = {
  color: PropTypes.string,
  size: PropTypes.oneOf([
    'm',
    'l',
    'xl'
  ]),
  busy: PropTypes.bool,
  className: PropTypes.string
}
const defaultProps = {
  color: '#333',
  size: 'l',
  busy: false,
  className: ''
}

export default class Places extends Component {
  makeClasses(init) {
      const { className, busy } = this.props;

      let classes = [className, init];

      if(busy) classes.push('busy')

      return classes.join(' ').trim();
  }
  render() {
    const {color} = this.props

    const styles = {
      backgroundColor: color
    }
    return (
      <div className={this.makeClasses('places-block')} style={styles}>

      </div>
    )
  }
}

Places.propTypes = propTypes
Places.defaultProps = defaultProps
