export const carsType = {
  sedan: 'sedan',
  disabled: 'disabled',
  truck: 'truck'
}

export const placesType = {
  sedan: {
    carTypes: [
      carsType.sedan,
      carsType.truck
    ],
    name: 'Sedan',
    color: 'rgb(189, 0, 163)'
  },
  disabled: {
    carTypes: [
      carsType.disabled,
      carsType.sedan,
      carsType.truck
    ],
    name: 'Disabled',
    color: 'rgb(81, 52, 157)'
  },
  truck: {
    carTypes: [
      carsType.truck
    ],
    name: 'Truck',
    color: 'rgb(111, 221, 145)'
  }
}

export const places = [
  {
    type: carsType.sedan,
    count: 30
  }, {
    type: carsType.disabled,
    count: 8
  }, {
    type: carsType.truck,
    count: 5
  }
]
