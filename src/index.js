import React from 'react'
import {render} from 'react-dom'
import 'reset-css/reset.css'
import App from './App'

render(<App/>, document.getElementById('app'))
