import React, {Component, PropTypes} from 'react'
import {placesType} from '../constants'
import './styles/parking.css'

import Place from '../components/Place'

const propTypes = {
  places: PropTypes.array,
  cars: PropTypes.array
}
const defaultProps = {
  places: [],
  cars: []
}

export default class Parking extends Component {
  renderParking() {
    const {places, cars} = this.props;
    return Object.keys(places).map(item => {
      return (
        <section className="parking-type-block" key={'parking' + item}>
          <h2>{placesType[item].name}</h2>
          {this.renderPlaces(places[item], cars[item], item)}
        </section>
      )
    })
  }
  renderPlaces(free = 0, busy = 0, type) {
    const color = placesType[type].color
    let freePlaces = []
    let busyPlaces = []
    if(free) {
      for(let i = 0; i < free; i++) {
        freePlaces.push(<Place color={color} key={'free' + i}/>)
      }
    }
    if(busy) {
      for(let i = 0; i < busy; i++) {
        busyPlaces.push(<Place color={color} key={'busy' + i} busy/>)
      }
    }

    return (
      <div className="parking-places-items">
        {busyPlaces}
        {freePlaces}
      </div>
    )
  }
  render() {
    return (
      <div className="parking-block">
        {this.renderParking()}
      </div>
    )
  }
}

Parking.propTypes = propTypes
Parking.defaultProps = defaultProps
