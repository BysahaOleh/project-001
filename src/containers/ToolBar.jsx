import React, {Component, PropTypes} from 'react'
import {carsType} from '../constants'
import './styles/toolbar.css'

const propTypes = {
  makeBusy: PropTypes.func,
  makeNotBusy: PropTypes.func
}
const defaultProps = {
  makeBusy: () => {
  },
  makeNotBusy: () => {
  }
}

export default class ToolBar extends Component {
  renderCarButton() {
    const {makeBusy, makeNotBusy} = this.props

    return Object.keys(carsType).map(item => {
      return (
        <div className="button-group">
          <span className="title">{item}</span>
          <a className="button" onClick={() => {
            makeBusy(item)
          }}>+</a>
          <a className="button"onClick={() => {
            makeNotBusy(item)
          }}>-</a>
        </div>
      )
    })
  }
  render() {
    return (
      <div className="tool-bar">
        {this.renderCarButton()}
      </div>
    )
  }
}

ToolBar.propTypes = propTypes
ToolBar.defaultProps = defaultProps
