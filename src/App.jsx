import React, {Component, PropTypes} from 'react'
import {places as placesCount, placesType} from './constants'
import './app.css'

import Parking from './containers/Parking'
import ToolBar from './containers/ToolBar'

export default class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      cars: {
        track: 0,
        sedan: 0,
        disabled: 0
      },
      places: {
        track: 0,
        sedan: 0,
        disable: 0
      },
      init: false
    }
  }

  componentWillMount() {
    this.initParking()
  }

  //Logger
  componentDidUpdate() {
    console.log(this.state)
  }
  componentDidMount() {
    console.log(this.state)
  }

  makeBusy(type) {
    let {cars, places} = this.state;

    for(let i = 0; i < placesType[type].carTypes.length; i++) {
      if(places[placesType[type].carTypes[i]]) {
        ++cars[placesType[type].carTypes[i]]
        --places[placesType[type].carTypes[i]]

        this.setState({
          cars,
          places
        })

        return
      }
    }
  }

  makeNotBusy(type) {
    let {cars, places} = this.state;

    if(cars[type]) {
      --cars[type]
      ++places[type]

      this.setState({
        cars,
        places
      })
    }
  }

  initParking() {
    let state = this.state
    let newState = {
      cars: {},
      places: {},
      init: true
    }

    placesCount.map((item) => {
      newState.cars[item.type] = 0
      newState.places[item.type] = item.count
    })

    this.setState(Object.assign({}, state, newState))
  }

  render() {
    let {init, places, cars} = this.state;
    return (
      <div className="app-block">
        <h1>Parking</h1>
        <ToolBar makeBusy={::this.makeBusy} makeNotBusy={::this.makeNotBusy}/>
        <hr/>
        {init ? <Parking places={places} cars={cars}/> : <div className="loading"/>}
      </div>
    )
  }
}
